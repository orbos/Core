# one time rebuild - README.

## Why?

I had was updating core:tools from core:tools and this ended up with the layers expanding a lot. This caused exponential growth in the time it took the update the images.

## What?

The below process is what I did to remove csl and dev-tools from the tools:core image. These were added before we had git access and can now just download the sources. This process creates a core:tools.tar archive of the docker container then creates a new image testcore:tools which we then build a new fresh registry.gitlab.com/orbos/core:tools image.

```
cd remove-dev-tools/
docker build --no-cache -t testcore:tools .
cd ../rebase-image
docker run -l CUSTOMLABEL testcore:tools sh -c 'echo 1'
docker export -o ./core:tools.tar $(docker ps -aqf 'label=CUSTOMLABEL')
docker rmi testcore:tools
docker import ${PWD}/core:tools.tar testcore:tools
docker build --no-cache -t registry.gitlab.com/orbos/core:tools .
rm -rfv ./core:tools.tar
```

## Improvements to prevent this from happening again.

This does not contain all future changes to the toolchain but its a good enough measure to fix the layer bloat that was being encountered. 

dev-tools now pulls the fresh core:tools image and builds core:dev-tools. If you pulled core:tools and then core:dev-tools this would only pull the changes between core:tools and core:dev-tools when updates to dev-tools happen.
