http://ftp.gnu.org/gnu/gcc/gcc-6.3.0/gcc-6.3.0.tar.bz2
dlurl
http://ftp.gnu.org/gnu/gcc/gcc-6.3.0/gcc-6.3.0.tar.bz2 http://www.mpfr.org/mpfr-3.1.5/mpfr-3.1.5.tar.xz http://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.xz http://www.multiprecision.org/mpc/download/mpc-1.0.3.tar.gz
http://gcc.gnu.org/
The GNU Compiler Collection.
mpfr gmp mpc




temp_pass1
Temporary toolchain pass 1 compile of gcc.

cs e ee dlf1 sbd c m mi
ee c


mpfr gmp mpc
../gcc-6.3.0/configure

--target=$ORBOS_TGT --prefix=/tools --with-glibc-version=2.11 --with-sysroot=$orbos --with-newlib --without-headers --with-local-prefix=/tools --with-native-system-header-dir=/tools/include --disable-nls --disable-shared --disable-multilib --disable-decimal-float --disable-threads --disable-libatomic --disable-libgomp --disable-libmpx --disable-libquadmath --disable-libssp --disable-libvtv --disable-libstdcxx --enable-languages=c,c++
y
temp_pass2
Temporary toolchain pass 2 compile of gcc.

cs e dlf2 ee sbd c m mi lngcc ctc
ee c ctc


mpfr gmp mpc
../gcc-6.3.0/configure
CC=$ORBOS_TGT-gcc CXX=$ORBOS_TGT-g++ AR=$ORBOS_TGT-ar RANLIB=$ORBOS_TGT-ranlib
--prefix=/tools --with-local-prefix=/tools --with-native-system-header-dir=/tools/include --enable-languages=c,c++ --disable-libstdcxx-pch --disable-multilib --disable-bootstrap --disable-libgomp


> gcc-tk-check
y
templibstdc
Temporary toolchain compile of libstdc++.

cs e sbd c m mi
c
../gcc-6.3.0/libstdc++-v3/configure

--host=$ORBOS_TGT --prefix=/tools --disable-multilib --disable-nls --disable-libstdcxx-threads --disable-libstdcxx-pch --with-gxx-include-dir=/tools/$ORBOS_TGT/include/c++/6.3.0
