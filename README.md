# Orbos Core Repository.

## Branches

### master

The master branch will be the branch that has the current working version of the Core packages and probably a core group telling the system which order to do the builds in.

This will most likely be a push from one of the version number branches.

### tools

This will create the temporary compilation tool chain used to compile the operating system.

### version number

These will essentially be the development branches and may have a version number-dev branch that then goes into version number-testing once all needed packages are added for people to test. Then once tested we push it to the version number branch.
